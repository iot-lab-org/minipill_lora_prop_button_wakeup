# MiniPill LoRa Hardware version 1.x

Please look at https://www.iot-lab.org/blog/370/ for general information on the starting project. In this this file I will share some software specific information.

## Special software version: Proprietary LoRAWan lib
In an earlier project I used a ATTiny84 with a RFM95W LoRa module https://www.iot-lab.org/blog/101/. I used and build special code to fit the 8k flash memory.

## Wakeup by using interrupt instead of timed interrupts.
This version is an example of code to use with interrupts. First message is always send.
Remember to use good debouncing hardware or solve the debounce problem in software
(not done in this code).
Very fast response. Low power usage in deepSleep mode, about 0.7uA.
