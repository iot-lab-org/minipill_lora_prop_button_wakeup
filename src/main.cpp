/*
  main.cpp
  Main code where the control takes place
  @author  Leo Korbee (c), Leo.Korbee@xs4all.nl
  @website iot-lab.org
  @license Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
  Thanks to all the folks who contributed beforme me on this code.

  Hardware information at the end of this file.

  @version 2020-11-27
  Added ISP.end in main code to disable ISP system. It saves about 66uA and
  reduced the total power usage to 1.3uA without BME280

  ====== External Interrupt version ======

  @version 2020-12-18
  Succesfull use of external interrupt. Very fast response and low power usage
  0.7uA in deepSleep mode.
  Timeout between successive interrupts is about 1 second. Remember to respect the
  LoRaWAN guidelines about transmission times.
  Use very good debounce elecronics or solve the debounce problem in software!

  @version 2020-12-21
  Added LED to sign that a signal was send.

  @version 2021-02-27 switched to V3 TTN stack

*/
#include <Arduino.h>
#include "LoRaWAN.h"
#include "STM32IntRef.h"
#include "STM32LowPower.h"
#include "secconfig.h" // remember to rename secconfig_example.h to secconfig.h and to modify this file

/*
  for debugging purposes, usualy not enough memory to use this with both BME
  and RFM95W
*/
// for debugging redirect to hardware Serial2
// Tx on PA2
//#define Serial Serial2
//HardwareSerial Serial2(USART2);   // or HardWareSerial Serial2 (PA3, PA2);

// RFM95W connection on MiniPill LoRa
#define DIO0 PA10
#define NSS  PA4
RFM95 rfm(SPI, DIO0, NSS);

// define LoRaWAN layer
LoRaWAN lora = LoRaWAN(rfm);
// frame counter for lora
unsigned int Frame_Counter_Tx = 0x0000;


// use a button as ExternalWakeup
#define WKUP_BTN PB11


// led for signing a packet is send
#define SIGNAL_LED PA1

// uses wakeupcall, does notthing
void wakeupFunction()
{
  // This function will be called once on device wakeup
  // You can do some little operations here (like changing variables which will be used in the loop)
  // Remember to avoid calling delay() and long running functions since this functions executes in interrupt context
}



// the things network stuff
// get them from the device overview page! uncomment and put this information here, remove the #include secconfig.h
// due to security reasons this information from the author is put in secconfig.h

// TTN, msb left
// unsigned char NwkSkey[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
// unsigned char AppSkey[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
// unsigned char DevAddr[4] = { 0x00, 0x00, 0x00, 0x00 };


// all functions declared
void disableIO();

void setup()
{
  // for debugging
  // Serial.begin(9600);

  // disableIO();

  // set WKUP_BTN pin to Input and PULL_UP
  pinMode(WKUP_BTN, INPUT_PULLUP);

  pinMode(SIGNAL_LED, OUTPUT);
  digitalWrite(SIGNAL_LED, HIGH);

  //Initialize RFM module
  rfm.init();
  lora.setKeys(NwkSkey, AppSkey, DevAddr);
  delay(500);

  // Configure low power at startup
  LowPower.begin();

  // Attach a wakeup interrupt on pin, calling wakeupFunction when the device is woken up
  LowPower.attachInterruptWakeup(WKUP_BTN, wakeupFunction, RISING);

  // use this delay for first packet send 8 seconds after reset
  delay(8000);
}

void loop()
{
  // define bytebuffer
  uint8_t Data_Length = 0x02;
  uint8_t Data[Data_Length];

  // read vcc voltage (mv)
  int32_t vcc = IntRef.readVref();
  Data[0] = (vcc >> 8) & 0xff;
  Data[1] = (vcc & 0xff);

  lora.Send_Data(Data, Data_Length, Frame_Counter_Tx);
  digitalWrite(SIGNAL_LED, LOW);
  delay(1000);
  digitalWrite(SIGNAL_LED, HIGH);

  Frame_Counter_Tx++;

  // disable SPI, reduces 67uA
  SPI.endTransaction();
  SPI.end();

  // go to low power mode
  LowPower.begin();

  // wait for Interrupt on interrupt pin/button
  //LowPower.deepSleep();
  // this is also possible, use both timed interrupt and external interrupt.
  // next timed interrupt is +settime after last interrupt, hardware or timed.
  LowPower.deepSleep(60000);

  // start SPI after sleep
  SPI.begin();
}


/*
  set unused pins so no undefined situation takes place
*/
void disableIO(void)
{
  GPIO_InitTypeDef GpioA;
  uint32_t gpio_pins = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3;
  GpioA.Pin = gpio_pins;
  GpioA.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(GPIOA, &GpioA);

  // No Reduction due to not using GpioB
  GPIO_InitTypeDef GpioB;
  gpio_pins = GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | \
                      GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7 | \
                      GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11 | \
                      GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15;
  GpioB.Pin = gpio_pins;
  GpioB.Mode = GPIO_MODE_ANALOG;
  HAL_GPIO_Init(GPIOB, &GpioB);

  __HAL_RCC_GPIOB_CLK_DISABLE();
  __HAL_RCC_GPIOC_CLK_DISABLE();
  __HAL_RCC_GPIOH_CLK_DISABLE();
}

/*
  Hardware setup
  MiniPill LoRa (iot-lab.org)
  STM32L051C8T6

  P2 connector
  	PA_0,  // Analog in
  	PA_1,  // Analog in   LED for signing a signal was send
  	PA_2,  // USART2_TX
  	PA_3,  // USART2_RX
  	VDD
  	GND
  	PA_4,  // SPI1_NSS   NSS - RFM95W
  	PA_5,  // SPI1_SCK.  SCK - RFM95W
  	PA_6,  // SPI1_MISO. MISO - RFM95W
  	PA_7,  // SPI1_MOSI. MOSI - RFM95W
  	VDD
  	GND

  P3 connector
  	PA_9,  // USART1_TX. RST - RFM95W
  	PA_10, // USART1_RX. DIO0 - RFM95W
  	PB_4,  //            DIO1 - RFM95W
  	PB_5,  //            DIO2 - RFM95W
  	PB_6,  // USART1_TX
  	PB_7,  // USART1_RX
  	PB_8,  // I2C1_SCL
  	PB_9,  // I2C1_SDA
  	PB_10, // LPUART1_TX
  	PB_11, // LPUART1_RX    WAKE UP BUTTON With hardware debounce (pull to GND)
  	VDD
  	GND


      PB11 // switch connected to ground with little debounce circuit:

      VCC
       |
      | | internal PULL_UP ristor, datasheet: about 40k
      | |
       |
      -+--- PB11 ---+---[ 4k7 ]----+
                    |              |
                    |              +- |
                  _____               |-|
                  _____ 100n       +- |
                    |              |
      GND-----------+--------------+

*/
